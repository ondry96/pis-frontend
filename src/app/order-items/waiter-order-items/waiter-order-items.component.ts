import { Component, OnInit } from '@angular/core';
import {OrderItemsService} from '../../../services/order-items/order-items.service';
import {
  CategoryOfMenuItems,
  CategoryOfOrderItems,
  MenuItemInCategory,
  OrderItemInCategory
} from '../order-items.model';
import {NotifierDisplayerUtil} from '../../../utils/NotifierDisplayerUtil';
import {ResponseErrorHandler} from '../../../utils/ResponseErrorHandler';
import {ActivatedRoute, Router} from '@angular/router';
import {ArrayItemManipulatorUtil} from '../../../utils/ArrayItemManipulatorUtil';
import Swal from 'sweetalert2';
import {SweetalertInfoSetterUtil} from '../../../utils/SweetalertInfoSetterUtil';

@Component({
  selector: 'app-waiter-order-items',
  templateUrl: './waiter-order-items.component.html',
  styleUrls: [
    './waiter-order-items.component.less',
    '../../menu/menu.less'
  ]
})
export class WaiterOrderItemsComponent implements OnInit {

  categoriesOrderItems: CategoryOfOrderItems[] = [];
  categoriesMenuItems: CategoryOfMenuItems[] = [];
  private placeId;
  private dataLoaded = false;

  constructor(private activatedRoute: ActivatedRoute,
              private router: Router,
              private orderItemsService: OrderItemsService) {
  }

  ngOnInit(): void {
    this.placeId = Number(this.activatedRoute.snapshot.paramMap.get('id'));
    if (isNaN(this.placeId)) {
      this.router.navigate(['/restaurant-scheme/']);
      return;
    }
    this.getOrderAndMenuItemsInCategories();
  }

  getOrderAndMenuItemsInCategories() {
    this.orderItemsService.getOrderAndMenuItemsInCategories(this.placeId).subscribe(
      res => {
        this.categoriesMenuItems = res.categoriesMenuItems;
        this.categoriesOrderItems = res.categoriesOrderItems;
        this.removePaidOrderItems();
        this.removeEmptyCategories();
        this.sortOrderItemsCategories();
        this.sortMenuItemsCategories();
        this.dataLoaded = true;
      },
      error => {
        ResponseErrorHandler.handleError(error);
      }
    );
  }

  showPopupAddSpecialRequirements(placeId: number, orderItem: OrderItemInCategory) {
    let requirementsValue;
    if (orderItem.specialRequirements !== undefined) {
      requirementsValue = orderItem.specialRequirements;
    } else {
      requirementsValue = '';
    }
    (async () => {
      const {value: requirements} = await Swal.fire({
        titleText: 'Zadejte speciální požadavky',
        input: 'textarea',
        inputPlaceholder: 'Zadejte speciální požadavky',
        inputValue: requirementsValue,
        inputAttributes: {
          'aria-label': 'Zadejte speciální požadavky'
        },
        showCancelButton: true,
        showCloseButton: true,
        confirmButtonText: 'Uložit',
        cancelButtonText: 'Zrušit',
        confirmButtonColor: '#87CB16',
        cancelButtonColor: '#FF4A55',
      });
      if (requirements !== undefined) {
        if (orderItem.state === 'WAITING') {
          this.orderItemsService.addSpecialRequirement(this.placeId, orderItem.orderItemId, requirements).subscribe(
            res => {
              orderItem.specialRequirements = requirements;
              NotifierDisplayerUtil.showNotification('success', 'Speciální požadavky byly změněny.');
            },
            error => {
              ResponseErrorHandler.handleError(error);
            }
          );
        } else {
          NotifierDisplayerUtil.showNotification('danger', 'Nelze upravit, objednávka již je připravována.');
        }
      }
    })();
  }

  addOrderItem(menuItem: MenuItemInCategory, category: CategoryOfMenuItems) {
    this.orderItemsService.addOrderItemToTableOrder(this.placeId, menuItem.menuItemId).subscribe(
      res => {
        NotifierDisplayerUtil.showNotification('success', 'Položka úspěšně přidána do objednávky.');
        const newOrderItem: OrderItemInCategory = {
          menuItemId: menuItem.menuItemId,
          name: menuItem.name,
          grammage: menuItem.grammage,
          price: menuItem.price,
          state: 'WAITING',
          specialRequirements: '',
          orderItemId: res.orderItemId
        };

        const categoryOfOrderItem = this.categoriesOrderItems.find(x => x.categoryName === category.categoryName);
        if (categoryOfOrderItem === undefined) {
          const newCategory: CategoryOfOrderItems = {
            orderItems: [newOrderItem],
            categoryName: category.categoryName
          };
          this.categoriesOrderItems.push(newCategory);
          this.sortOrderItemsCategories();
        } else {
          categoryOfOrderItem.orderItems.push(newOrderItem);
        }
      },
      error => {
        ResponseErrorHandler.handleError(error);
      }
    );
  }

  removeOrderItem(orderItem: OrderItemInCategory, category: CategoryOfOrderItems) {
    Swal.fire(
      SweetalertInfoSetterUtil.getInfoForDeleteAlert('Opravdu chcete zrušit objednávku na ' + orderItem.name + '?')
    ).then((result) => {
      if (result.value) {
        if(orderItem.state === 'WAITING') {
          this.orderItemsService.deleteOrderItem(this.placeId, orderItem.orderItemId).subscribe(
            res => {
              ArrayItemManipulatorUtil.deleteItemFromArray(orderItem, category.orderItems);
              NotifierDisplayerUtil.showNotification('success', 'Položka úspěšně odebrána z objednávky.');
              if (category.orderItems.length < 1) {
                ArrayItemManipulatorUtil.deleteItemFromArray(category, this.categoriesOrderItems);
              }
            },
            error => {
              ResponseErrorHandler.handleError(error);
            }
          );
        } else {
          NotifierDisplayerUtil.showNotification('danger', 'Nelze zrušit, objednávka již je připravována.');
        }
      }
    });
  }

  private removeEmptyCategories() {
    this.categoriesMenuItems = this.categoriesMenuItems.filter(x => x.menuItems.length > 0);
    this.categoriesOrderItems = this.categoriesOrderItems.filter(x => x.orderItems.length > 0);
  }

  private sortMenuItemsCategories() {
    this.categoriesMenuItems.sort((
      (a, b) =>
        a.categoryName.localeCompare(b.categoryName)));
  }

  private sortOrderItemsCategories() {
    this.categoriesOrderItems.sort((
      (a, b) =>
        a.categoryName.localeCompare(b.categoryName)));
  }

  private removePaidOrderItems() {
    for (const category of this.categoriesOrderItems) {
      for (const item of category.orderItems) {
        if (item.state === 'PAID') {
          ArrayItemManipulatorUtil.deleteItemFromArray(item, category.orderItems);
        }
      }
    }
  }

  private getState(state: string) {
    switch (state) {
      case 'WAITING': {
        return 'Čeká na vyřízení';
      }
      case 'PREPARING': {
        return 'V přípravě';
      }
      case 'FINISHED': {
        return 'Připraveno k výdeji';
      }
      case 'RELEASED': {
        return 'Vydáno';
      }
      case 'PAID': {
        return 'Zaplaceno';
      }
      default: {
        console.log('Wrong state!');
      }
    }
  }
}
