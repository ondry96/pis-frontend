export interface MealsToPay {
  categoryName: string;
  receiptsItems: [
    ReceiptsItem
  ];
}

export interface ReceiptsItem {
  orderItemId: number;
  menuItemId: number;
  name: string;
  grammage: string;
  price: number;
  specialRequirements: string;
}

export interface OrderItemIds {
  orderItemIds: number[];
}
