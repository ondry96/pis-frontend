import { Component, OnInit } from '@angular/core';
import {NotFoundConstants} from './not-found.constants';
import {AuthenticationService} from '../../services/authentication/authentication.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-not-found',
  templateUrl: './not-found.component.html',
  styleUrls: ['./not-found.component.less']
})
export class NotFoundComponent implements OnInit {
  private readonly notFoundConstants: typeof NotFoundConstants = NotFoundConstants;

  constructor(private authenticationService: AuthenticationService, private router: Router) {

  }

  ngOnInit() {
  }

  notFoundRedirect() {
    this.authenticationService.getHomepage(this.router);
  }
}
