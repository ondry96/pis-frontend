import {Component, OnInit} from '@angular/core';
import {LoginModel} from './login.model';
import {LoginConstants} from './login.constants';
import {AuthenticationService} from '../../../services/authentication/authentication.service';
import {Router} from '@angular/router';
import {LoginResponseModel} from '../../response.model';
import {ResponseErrorHandler} from '../../../utils/ResponseErrorHandler';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: [
    './login.component.less',
    '../authentication.less'
  ]
})
export class LoginComponent implements OnInit {
  private readonly loginConstants: typeof LoginConstants = LoginConstants;

  loginModel: LoginModel = {
    email: '',
    password: ''
  };

  constructor(private authenticationService: AuthenticationService, private router: Router) {}
  ngOnInit() {
  }

  sendLoginData() {
    (document.getElementById('submitButton') as HTMLButtonElement).disabled = true;
    this.authenticationService.sendLoginData(this.loginModel).subscribe(
      res => {
        this.fillLocalStorage(res, this.loginModel.email);
        this.authenticationService.getHomepage(this.router);
      },
      error => {
        ResponseErrorHandler.handleError(error);
        (document.getElementById('submitButton') as HTMLButtonElement).disabled = false;
      }
    );
  }


 private fillLocalStorage(user: LoginResponseModel, email: string) {
   localStorage.clear();
   localStorage.setItem(this.authenticationService.authConstants.LOGGED_EMAIL, email);
   localStorage.setItem('token', user.token);

   for (const entry of this.authenticationService.authConstants.USER_NAME_HASH_ARRAY) {
      if (user.roles.find(s  => s === entry.name)) {
        localStorage.setItem(entry.name, entry.hash);
        if (entry.name === this.authenticationService.authConstants.LOGGED_USER_NAME) {
          return;
        }
      }
    }
  }

}
