import { Component, OnInit } from '@angular/core';
import {AuthenticationService} from '../../../services/authentication/authentication.service';
import {NotifierDisplayerUtil} from '../../../utils/NotifierDisplayerUtil';
import {EditProfileConstants} from './edit-profile.constants';
import {EditProfileModel} from './edit-profile.model';
import {Router} from '@angular/router';
import {ResponseErrorHandler} from '../../../utils/ResponseErrorHandler';

@Component({
  selector: 'app-edit-profile',
  templateUrl: './edit-profile.component.html',
  styleUrls: [
    './edit-profile.component.less',
    '../authentication.less'
  ]
})
export class EditProfileComponent implements OnInit {
  private readonly editProfileConstants: typeof EditProfileConstants = EditProfileConstants;
  private dataLoaded = false;
  editProfile: EditProfileModel = {
    name: '',
    surname: '',
    email: '',
    oldPassword: '',
    password: ''
  };

  repeatedPassowrd: '';

  constructor(private authenticationService: AuthenticationService, private router: Router) { }

  ngOnInit() {
    this.getUserData();
  }

  getUserData() {
    this.authenticationService.getUserData().subscribe(
      res => {
        this.editProfile = res;
        this.dataLoaded = true;
      },
      error => {
        ResponseErrorHandler.handleError(error);
      }
    );
  }

  updateUserData() {
    (document.getElementById('submitButtonEdit') as HTMLButtonElement).disabled = true;
    const profileToEdit: EditProfileModel = {
      name: this.editProfile.name,
      surname: this.editProfile.surname,
      email: this.editProfile.email,
      password: this.editProfile.password,
      oldPassword: this.editProfile.oldPassword,
    };

    if (profileToEdit.password !== undefined && profileToEdit.oldPassword !== undefined) {
      const hashPassword = this.authenticationService.hashPassword(profileToEdit.password);
      const hashOldPassword = this.authenticationService.hashPassword(profileToEdit.oldPassword);
      profileToEdit.password = hashPassword;
      profileToEdit.oldPassword = hashOldPassword;
    }
    this.authenticationService.updateUserData(profileToEdit, localStorage.getItem('loggedEmail')).subscribe(
      res => {
        this.router.navigate(['/account']);
        localStorage.setItem('loggedEmail', this.editProfile.email);
        localStorage.setItem('token', res.token);
        NotifierDisplayerUtil.showNotification('success', 'Udaje byly zmeneny.');
      },
      error => {
        ResponseErrorHandler.handleError(error);
      }
    );
    (document.getElementById('submitButtonEdit') as HTMLButtonElement).disabled = false;
  }

  private arePasswordsSame() {
    if (this.repeatedPassowrd !== undefined && this.editProfile.password !== undefined) {
      return this.repeatedPassowrd.toString() === this.editProfile.password.toString();
    }
    return true;
  }
}
