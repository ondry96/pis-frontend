export class EditProfileConstants {
  public static readonly EDIT_PROFILE_BUTTON = 'Změnit údaje';
  public static readonly EDIT_PROFILE_TITLE = 'Editace osobních údajů';
  public static readonly WRONG_EMAIL_FORMAT = 'Zadejte email ve správném tvaru';
  public static readonly PASSWORDS_NOT_SAME = 'Hesla se musí shodovat';
  public static readonly PASSWORD_TOO_SHORT = 'Heslo musí mít nejméně 5 znaků';
  public static readonly REQUIRED_FIELD = 'Toto pole je povinné';
}
