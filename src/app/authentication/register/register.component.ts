import {Component, OnInit} from '@angular/core';
import {RegisterConstants} from './register.constants';
import {RegisterModel} from './register.model';
import {AuthenticationService} from '../../../services/authentication/authentication.service';
import {Router} from '@angular/router';
import {ResponseErrorHandler} from '../../../utils/ResponseErrorHandler';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: [
    './register.component.less',
    '../authentication.less'
  ]
})
export class RegisterComponent implements OnInit {
  private readonly registerConstants: typeof RegisterConstants = RegisterConstants;

  repeatedPasswordModel: '';
  user: RegisterModel = {
    name: '',
    surname: '',
    email: '',
    password: '',
  };


  constructor(private authenticationService: AuthenticationService, private router: Router) {
  }

  ngOnInit() {
  }

  sendRegisterData() {
    (document.getElementById('submitButton') as HTMLButtonElement).disabled = true;
    this.authenticationService.sendRegisterData(this.user).subscribe(
      res => {
        localStorage.setItem('token', res.token);
        localStorage.setItem(this.authenticationService.authConstants.LOGGED_USER_NAME,
                               this.authenticationService.authConstants.LOGGED_USER_HASH);
        localStorage.setItem(this.authenticationService.authConstants.LOGGED_EMAIL,
                               this.user.email);
        this.router.navigate(['/reservations']);
      },
      error => {
        ResponseErrorHandler.handleError(error);
        (document.getElementById('submitButton') as HTMLButtonElement).disabled = false;
      }
    );
  }

  arePasswordsSame() {
    return this.repeatedPasswordModel.toString() === this.user.password.toString();
  }
}
