export class RegisterConstants {
  public static readonly REQUIRED_FIELD = 'Toto pole je povinné';
  public static readonly CREATE_ACCOUNT_BUTTON = 'Vytvořit účet';
  public static readonly WRONG_EMAIL_FORMAT = 'Zadejte email ve správném tvaru';
  public static readonly PASSWORDS_NOT_SAME = 'Hesla se musí shodovat';
  public static readonly PASSWORD_TOO_SHORT = 'Heslo musí mít nejméně 5 znaků';
  public static readonly CREATE_ACCOUNT = 'Registrace účtu';
}
