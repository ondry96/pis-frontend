import {Component, OnInit} from '@angular/core';
import {SchemeModel} from '../../reservation/create-reservation/restaurant-reservation-scheme/scheme.model';
import {SchemeService} from '../../../services/scheme/scheme.service';
import {ResponseErrorHandler} from '../../../utils/ResponseErrorHandler';
import Swal from 'sweetalert2';
import {Router} from '@angular/router';
import {OrderItemsService} from '../../../services/order-items/order-items.service';
import {OrderItem} from '../../order-items/order-items.model';

@Component({
  selector: 'app-restaurant-scheme',
  templateUrl: './restaurant-waiter-scheme.component.html',
  styleUrls: ['./restaurant-waiter-scheme.component.less']
})
export class RestaurantWaiterSchemeComponent implements OnInit {

  constructor(
    private schemeService: SchemeService,
    private router: Router,
    private orderItemsService: OrderItemsService) {
  }

  schemeTablesModel: SchemeModel[] = [];
  schemeLoungesModel: SchemeModel[] = [];
  orderItems: OrderItem[] = [];

  private tablesReady = false;
  private loungesReady = false;
  private styleSet = false;

  private static getElementName(placeId: number) {
    if (placeId === 20 || placeId === 21) {
      return 'lounge';
    } else {
      return 'table';
    }
  }

  ngOnInit(): void {
    this.getAllTables();
    this.getAllLounges();
    this.getFreeTables();
  }

  getAllTables() {
    this.schemeService.getAllTables().subscribe(
      res => {
        this.schemeTablesModel = res;
        this.tablesReady = true;
      },
      error => {
        ResponseErrorHandler.handleError(error);
      }
    );
  }

  getAllLounges() {
    this.schemeService.getAllLounges().subscribe(
      res => {
        this.schemeLoungesModel = res;
        this.loungesReady = true;
      },
      error => {
        ResponseErrorHandler.handleError(error);
      }
    );
  }

  getFreeTables() {
    if (this.styleSet) {
      return;
    }
    this.orderItemsService.getOrderItemsListData().subscribe(
      res => {
        this.orderItems = res;
        this.orderItems = this.orderItems.filter(item => item.state !== 'PAID');
        let elementName;
        for (const order of this.orderItems) {
          elementName = RestaurantWaiterSchemeComponent.getElementName(order.placeId);

          (document.getElementById(elementName + '-' + order.placeId) as HTMLElement).classList.remove('place-no-order');
          (document.getElementById(elementName + '-' + order.placeId) as HTMLElement).classList.add('place-order');
        }
      },
      error => {
        ResponseErrorHandler.handleError(error);
      }
    );
    this.styleSet = true;
  }

  selectPlace(placeId: number) {
    if (!this.orderItems.find(ob => ob.placeId === placeId)) {
      this.router.navigate(['order/' + placeId]);
    } else {
      const customSwal = Swal.mixin({
        customClass: {
          confirmButton: 'btn btn-primary btn-fill popup-button-style',
          cancelButton: 'btn btn-success btn-fill popup-button-style'
        },
        buttonsStyling: false,
        width: 380,
      });

      customSwal.fire({
        showCancelButton: true,
        showCloseButton: true,
        confirmButtonText: '<i class="popup-icon-style fas fa-list-ul"></i><br>Objednávka',
        cancelButtonText: '<i class="popup-icon-style fas fa-dollar-sign"></i><br>Platba'
      }).then((result) => {
        if (result.value) {
          this.router.navigate(['order/' + placeId]);
        } else if (result.dismiss === Swal.DismissReason.cancel) {
          this.router.navigate(['payment/' + placeId]);
        }
      });
    }
  }
}
