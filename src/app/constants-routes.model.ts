export class ConstantsRoutesModel {

  private static BACKEND_BASE_URL = 'http://localhost:12345';

  /*authentication.service*/
  public static readonly REGISTER_URL = `${ConstantsRoutesModel.BACKEND_BASE_URL}\\register`;
  public static readonly LOGIN_URL = `${ConstantsRoutesModel.BACKEND_BASE_URL}\\login`;
  public static readonly EDIT_PROFILE_GET = `${ConstantsRoutesModel.BACKEND_BASE_URL}\\account`;
  public static readonly EDIT_PROFILE_PUT = `${ConstantsRoutesModel.BACKEND_BASE_URL}\\account\\`;

  /*employees.service*/
  public static readonly NEW_EMPLOYEE_URL = `${ConstantsRoutesModel.BACKEND_BASE_URL}\\employees\\new`;
  public static readonly EDIT_EMPLOYEES_GET_URL = `${ConstantsRoutesModel.BACKEND_BASE_URL}\\employees\\edit`;
  public static readonly EMPLOYEE_BASE_URL = `${ConstantsRoutesModel.BACKEND_BASE_URL}\\employee\\`;
  public static readonly DELETE = `\\delete`;
  public static readonly CHANGE_ROLE_STATE = `\\changeRoleState`;
  public static readonly EDIT_ATTRIBUTE = `\\editAttribute`;

  /*reservation.service*/
  public static readonly ALL_RESERVATIONS = `${ConstantsRoutesModel.BACKEND_BASE_URL}\\reservations`;
  public static readonly CHANGE_RESERVATION_STATE = `${ConstantsRoutesModel.BACKEND_BASE_URL}\\reservations\\`;
  public static readonly NEW_RESERVATION_UNLOGGED_URL = `${ConstantsRoutesModel.BACKEND_BASE_URL}\\reservations\\new\\unlogged`;
  public static readonly NEW_RESERVATION_LOGGED_GET_FREE_PLACES =
    `${ConstantsRoutesModel.BACKEND_BASE_URL}\\reservations\\logged\\free-places`;
  public static readonly NEW_RESERVATION_LOGGED_FINAL_STEP = `${ConstantsRoutesModel.BACKEND_BASE_URL}\\reservations\\new\\scheme`;
  public static readonly DELETE_RESERVATION_VIA_CODE_URL = `${ConstantsRoutesModel.BACKEND_BASE_URL}\\reservations\\code`;

  /*menu.service*/
  public static readonly CATEGORIES = `${ConstantsRoutesModel.BACKEND_BASE_URL}\\menu\\categories\\`;
  public static readonly EDIT_MENU = `${ConstantsRoutesModel.BACKEND_BASE_URL}\\menu\\edit`;
  public static readonly MENU_ITEMS = `${ConstantsRoutesModel.BACKEND_BASE_URL}\\menu_items\\`;
  public static readonly ADD_MENU_ITEM = `${ConstantsRoutesModel.BACKEND_BASE_URL}\\menu_items\\new`;
  public static readonly MENU_ITEMS_IN_MENU = `${ConstantsRoutesModel.BACKEND_BASE_URL}\\menu\\`;

  /*scheme.service*/
  public static readonly GET_ALL_TABLES = `${ConstantsRoutesModel.BACKEND_BASE_URL}\\schemes\\tables`;
  public static readonly GET_ALL_LOUNGES = `${ConstantsRoutesModel.BACKEND_BASE_URL}\\schemes\\lounges`;

  /*order-items.service*/
  public static readonly ORDER_ITEMS = `${ConstantsRoutesModel.BACKEND_BASE_URL}\\order_items\\`;
  public static readonly PREPARING = `\\preparing`;
  public static readonly FINISHED = `\\finished`;
  public static readonly RELEASED = `\\released`;

  public static readonly PLACES =  `${ConstantsRoutesModel.BACKEND_BASE_URL}\\places\\`;
  public static readonly ADD_ORDER_ITEM = `\\add_order_item\\`;
  public static readonly WAITER_ORDER_ITEMS = `\\order_items\\`;

  /*billing.service*/
  public static readonly BILLING_ITEMS = `${ConstantsRoutesModel.BACKEND_BASE_URL}\\places\\`;
  public static readonly PAYMENT = `\\payment`;
  public static readonly REQUIREMENT = `\\requirement`;

  /*sales.service*/
  public static readonly SALES = `${ConstantsRoutesModel.BACKEND_BASE_URL}\\sales\\`;
  public static readonly GET_COUNT_OF_SOLD_MENU_ITEMS_URL = `${ConstantsRoutesModel.SALES}count`;
  public static readonly GET_TIMES_OF_SALES_OF_MENU_ITEMS_URL = `${ConstantsRoutesModel.SALES}timeline`;
  public static readonly GET_LAST_TIME_WHEN_MENU_ITEM_WAS_SOLD_URL = `${ConstantsRoutesModel.SALES}last`;
  public static readonly GET_UTILIZATION = `${ConstantsRoutesModel.SALES}get_average_utilization`;

}
