export class BasicMenuItem {
  name: string;
  price: number;
  productionPrice: number;
  grammage: string;
  description: string;
  inMenu: number;
  menuItemId: number;
}

export interface SoldMenuItem extends BasicMenuItem {
  soldCount: number;
}

export interface MenuItemSalesTimeline extends BasicMenuItem {
  soldTimes: string[];
}

export interface MenuItemSalesLastTime extends BasicMenuItem {
  lastTimeSold: string;
}

export interface AverageUtilization {
  'hour': number;
  'RatioNumberOfConcreteHour': number;
}

export interface MenuItemsSalesLastTimeData {
  name: string;
  date: Date;
}
