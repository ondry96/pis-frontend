export class ReservationsConstants {
  public static readonly RESERVATIONS_HEADER = 'Rezervace';
  public static readonly NO_RESERVATION = 'V databázi se nenachází žádná rezervace.';
  public static readonly CANCELED_RESERVATION = 'Tato rezervace byla zrušena';
  public static readonly NEW_RESERVATION = 'Vytvořit novou rezervaci';
}
