import { Component, OnInit } from '@angular/core';
import {ReservationService} from '../../../services/reservation/reservation.service';
import {DeleteReservationCodeModel} from './delete-reservation-code.model';
import {ResponseErrorHandler} from '../../../utils/ResponseErrorHandler';
import {NotifierDisplayerUtil} from '../../../utils/NotifierDisplayerUtil';

@Component({
  selector: 'app-delete-reservation-code',
  templateUrl: './delete-reservation-code.component.html',
  styleUrls: ['./delete-reservation-code.component.less']
})
export class DeleteReservationCodeComponent implements OnInit {

  codeForDelete: DeleteReservationCodeModel = {
    code: ''
  };

  constructor(private reservationService: ReservationService) {

  }

  ngOnInit(): void {
  }

  sendCodeForDeleteReservation() {
    this.reservationService.sendCodeForDeleteReservation(this.codeForDelete).subscribe(
      () => {
          NotifierDisplayerUtil.showNotification('success', 'Rezervace byla úspěšně smazána.')
      },
      error => {
        ResponseErrorHandler.handleError(error);
      }
    );
  }

}
