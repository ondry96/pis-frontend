import { Component, OnInit } from '@angular/core';
import {CreateReservationConstants} from '../create-reservation.constants';
import {ReservationService} from '../../../../services/reservation/reservation.service';
import {Router} from '@angular/router';
import {DatePipe} from '@angular/common';
import {ReservationInfo} from '../create-reservation.model';
import {ResponseErrorHandler} from '../../../../utils/ResponseErrorHandler';
import {GetFreeAndOccupiedTablesResponse} from '../../../response.model';
import {ReservationDataStorage} from '../../../../services/reservation/reservation-data-storage.service';

@Component({
  selector: 'app-create-reservation-logged',
  templateUrl: './create-reservation-logged.component.html',
  styleUrls: ['../create-reservation.less'],
  providers: [DatePipe]
})
export class CreateReservationLoggedComponent implements OnInit {
  private readonly createReservationConstants: typeof CreateReservationConstants = CreateReservationConstants;

  reservationInfo: ReservationInfo = {
      date: '',
      time: '',
      expectedLength: 0,
      hostsCount: 0
    };

  places: GetFreeAndOccupiedTablesResponse;

  constructor(private reservationService: ReservationService,
              private router: Router,
              private datePipe: DatePipe,
              private dataStorage: ReservationDataStorage) {
  }

  ngOnInit(): void {
    this.dataStorage.fillDataStorage(null, null);
  }

  sendFirstStepAndGetPlaces() {
    this.reservationService.sendFirstStepReservationLogged(this.reservationInfo).subscribe(
      res => {
        this.places = res;
        this.dataStorage.fillDataStorage(this.places, this.reservationInfo);
        this.router.navigate(['reservation/restaurant-scheme']);
      },
      error => {
        ResponseErrorHandler.handleError(error);
      }
    );
  }

  isDateRight() {
    return this.reservationService.isDateRight(this.reservationInfo.date, this.datePipe);
  }

  isHostsCountRight() {
    return this.reservationService.isHostsCountRight(this.reservationInfo.hostsCount);
  }

  isTimeRight() {
    return this.reservationService.isTimeRight(this.reservationInfo.date, this.reservationInfo.time, this.datePipe);
  }

  isTimeInOpeningHours() {
    return this.reservationService.isTimeInOpeningHours(this.reservationInfo.time);
  }

  isTimeBeforeClosing() {
    return !this.reservationService.isTimeBeforeClosing(this.reservationInfo.time);
  }
}
