import {AfterViewChecked, Component, OnDestroy, OnInit} from '@angular/core';
import {SchemeModel} from './scheme.model';
import {SchemeService} from '../../../../services/scheme/scheme.service';
import {ResponseErrorHandler} from '../../../../utils/ResponseErrorHandler';
import Swal from 'sweetalert2';
import {SweetalertInfoSetterUtil} from '../../../../utils/SweetalertInfoSetterUtil';
import {ReservationDataStorage} from '../../../../services/reservation/reservation-data-storage.service';
import {GetFreeAndOccupiedTablesResponse} from '../../../response.model';
import {FinalReservation, ReservationInfo} from '../create-reservation.model';
import {Router} from '@angular/router';
import {ReservationService} from '../../../../services/reservation/reservation.service';
import {AuthenticationService} from '../../../../services/authentication/authentication.service';

@Component({
  selector: 'app-scheme',
  templateUrl: './reservation-scheme.component.html',
  styleUrls: ['./reservation-scheme.component.less']
})
export class ReservationSchemeComponent implements OnInit, OnDestroy, AfterViewChecked {
  schemeTablesModel: SchemeModel[] = [];
  schemeLoungesModel: SchemeModel[] = [];

  places: GetFreeAndOccupiedTablesResponse;
  reservationInfo: ReservationInfo;
  private styleSet = false;
  private tablesReady = false;
  private loungesReady = false;

  finalReservation: FinalReservation = {
    reservationInfo: {
      date: '',
      time: '',
      expectedLength: 0,
      hostsCount: 0
    },
    hostInfo: {
      email: localStorage.getItem('loggedEmail'),
      name: '',
      surname: ''
    },
    placeId: 0
  };

  constructor(private schemeService: SchemeService,
              private reservationService: ReservationService,
              private dataStorage: ReservationDataStorage,
              private router: Router,
              private authService: AuthenticationService) {
  }

  private static getElementName(placeId: number) {
    if (placeId === 20 || placeId === 21) {
      return 'lounge';
    } else {
      return 'table';
    }
  }


  ngOnInit(): void {
    try {
      // check if data storage is fulfilled
      this.places = this.dataStorage.data.places;
    } catch (e) {
      this.router.navigate(['reservation']);
      return;
    }
    this.reservationInfo = this.dataStorage.data.reservationInfo;
    this.getAllTables();
    this.getAllLounges();
  }

  ngAfterViewChecked(): void {
    this.setStylesToPlaces();
  }

  ngOnDestroy(): void {
    this.dataStorage.fillDataStorage(null, null);
  }

  getAllTables() {
    this.schemeService.getAllTables().subscribe(
      res => {
        this.schemeTablesModel = res;
        this.tablesReady = true;
      },
      error => {
        ResponseErrorHandler.handleError(error);
      }
    );
  }

  getAllLounges() {
    this.schemeService.getAllLounges().subscribe(
      res => {
        this.schemeLoungesModel = res;
        this.loungesReady = true;
      },
      error => {
        ResponseErrorHandler.handleError(error);
      }
    );
  }

  selectPlace(placeId: number) {
    if (!this.places.freeTables.includes(placeId)) {
      return;
    }
    Swal.fire(
      SweetalertInfoSetterUtil.getConfirmAlert('Opravdu chcete vytvořit rezervaci na místo ' + placeId + '?')
    ).then((result) => {
      if (result.value) {
        if (this.authService.hasUserSpecificRole(this.authService.authConstants.WAITER_NAME)) {
          (async () => {
            const {value: formValues} = await Swal.fire({
              title: 'Zadejte informace o hostovi',
              html:
                '<div class="form-group" id="popup-email-group">' +
                ' <label for="popup-email" style="float: left; padding-left: 5px" style="">' +
                '<i class="fa fa-envelope"></i>&nbsp;Emailová adresa</label>' +
                ' <input placeholder="Email" id="popup-email" class="swal2-input" style="margin: 0">' +
                '</div>' +
                '<div class="form-group" id="popup-name-group">' +
                ' <label for="popup-name" style="float: left; padding-left: 5px"><i class="fa fa-user"></i>&nbsp;Křestní jméno' +
                '<span class="mandatory">*</span></label>' +
                ' <input placeholder="Jméno" id="popup-name" class="swal2-input" style="margin: 0">' +
                '</div>' +
                '<div class="form-group" id="popup-surname-group">' +
                ' <label for="popup-surname" style="float: left; padding-left: 5px"><i class="fa fa-user"></i>&nbsp;Příjmení' +
                '<span class="mandatory">*</span></label>' +
                ' <input placeholder="Příjmení" id="popup-surname" class="swal2-input" style="margin: 0">' +
                '</div>',
              focusConfirm: false,
              preConfirm: () => {
                const email = (document.getElementById('popup-email') as HTMLInputElement).value;
                const name = (document.getElementById('popup-name') as HTMLInputElement).value;
                const surname =  (document.getElementById('popup-surname') as HTMLInputElement).value;
                if (name === '' || surname === '') {
                  Swal.showValidationMessage(`Pole pro jméno nebo příjmení je prázdné`);
                } else if (email !== '' && !this.validateEmail(email)) {
                  Swal.showValidationMessage(`Zadejte email ve správném tvaru`);
                }
                return {email, name, surname} ;
              }
            });
            this.finalReservation.hostInfo.email = formValues.email;
            this.finalReservation.hostInfo.name = formValues.name;
            this.finalReservation.hostInfo.surname = formValues.surname;
            this.sendNewReservation(placeId);
          })();
        } else {
          this.sendNewReservation(placeId);
        }
      }
    });
  }

  sendNewReservation(placeId: number) {
    this.finalReservation.reservationInfo = this.dataStorage.data.reservationInfo;
    this.finalReservation.placeId = placeId;
    SweetalertInfoSetterUtil.waitForReservationCreation('load', null);
    this.reservationService.sendFullReservationLoggedOrWaiter(this.finalReservation).subscribe(
      () => {
        SweetalertInfoSetterUtil.waitForReservationCreation(true, 'Rezervace byla úspěšně vytvořena.');
        this.router.navigate(['reservations']);
      },
      error => {
        SweetalertInfoSetterUtil.waitForReservationCreation(false, error);
      }
    );
  }

  setStylesToPlaces() {
    if (this.styleSet) {
      return;
    }
    let elementName;

    for (const freePlace of this.places.freeTables) {
      elementName = ReservationSchemeComponent.getElementName(freePlace);

      (document.getElementById(elementName + '-' + freePlace) as HTMLElement).classList.remove('place-not-available');
      (document.getElementById(elementName + '-' + freePlace) as HTMLElement).classList.add('place-free');
      (document.getElementById(elementName + '-' + freePlace) as HTMLElement).classList.add('place-hover');
    }

    for (const occupiedPlace of this.places.occupiedTables) {
      elementName = ReservationSchemeComponent.getElementName(occupiedPlace);
      (document.getElementById(elementName + '-' + occupiedPlace) as HTMLElement).classList.remove('place-not-available');
      (document.getElementById(elementName + '-' + occupiedPlace) as HTMLElement).classList.add('place-occupied');
    }
    this.styleSet = true;
  }

  validateEmail(email) {
    // tslint:disable-next-line:max-line-length
    const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
  }
}
