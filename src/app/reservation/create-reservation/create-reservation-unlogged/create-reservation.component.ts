import {Component, OnInit} from '@angular/core';
import {CreateReservationConstants} from '../create-reservation.constants';
import {CreateReservationModel} from '../create-reservation.model';
import {Router} from '@angular/router';
import {ReservationService} from '../../../../services/reservation/reservation.service';
import {DatePipe} from '@angular/common';
import {SweetalertInfoSetterUtil} from '../../../../utils/SweetalertInfoSetterUtil';

@Component({
  selector: 'app-create-reservation',
  templateUrl: './create-reservation.component.html',
  styleUrls: ['../create-reservation.less'],
  providers: [DatePipe]
})
export class CreateReservationComponent implements OnInit {
  private readonly createReservationConstants: typeof CreateReservationConstants = CreateReservationConstants;

  reservation: CreateReservationModel = {
    hostInfo: {
      email: '',
      name: '',
      surname: '',
    },
    reservationInfo: {
      date: '',
      time: '',
      expectedLength: 0,
      hostsCount: 0,
    }
  };

  constructor(private reservationService: ReservationService, private router: Router, private datePipe: DatePipe) {
  }

  ngOnInit() {
  }

  sendNewReservationData() {
    (document.getElementById('submitButton') as HTMLButtonElement).disabled = true;
    SweetalertInfoSetterUtil.waitForReservationCreation('load', null);
    this.reservationService.sendNewReservationUnloggedUserData(this.reservation).subscribe(
      () => {
        SweetalertInfoSetterUtil.waitForReservationCreation(true, 'Rezervace byla úspěšně vytvořena.');
        this.router.navigate(['/homepage']);
      },
      error => {
        SweetalertInfoSetterUtil.waitForReservationCreation(false, error);
        (document.getElementById('submitButton') as HTMLButtonElement).disabled = false;
      }
    );
  }

  isDateRight() {
    return this.reservationService.isDateRight(this.reservation.reservationInfo.date, this.datePipe);
  }

  isHostsCountRight() {
    return this.reservationService.isHostsCountRight(this.reservation.reservationInfo.hostsCount);
  }

  isTimeRight() {
    return this.reservationService.isTimeRight(this.reservation.reservationInfo.date, this.reservation.reservationInfo.time, this.datePipe);
  }

  isTimeInOpeningHours() {
    return this.reservationService.isTimeInOpeningHours(this.reservation.reservationInfo.time);
  }

  isTimeBeforeClosing() {
    return !this.reservationService.isTimeBeforeClosing(this.reservation.reservationInfo.time);
  }
}
