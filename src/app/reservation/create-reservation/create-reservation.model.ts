export interface CreateReservationModel {
  hostInfo: HostInfo;
  reservationInfo: ReservationInfo;
}

export class HostInfo {
  email: string;
  name: string;
  surname: string;
}

export class ReservationInfo {
  date: string;
  time: string;
  expectedLength: number;
  hostsCount: number;
}

export class FinalReservation {
  reservationInfo: ReservationInfo;
  hostInfo: HostInfo;
  placeId: number;
}
