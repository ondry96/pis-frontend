export class CreateReservationConstants {
  public static readonly REQUIRED_FIELD = 'Toto pole je povinné';
  public static readonly CREATE_RESERVATION = 'Vytvoření rezervace';
  public static readonly WRONG_EMAIL_FORMAT = 'Zadejte email ve správném tvaru';
  public static readonly BAD_HOST_COUNT = 'Rezervaci lze vytvořit nejméně pro 1 hosta, nebo maximálně 40 hostů';
  public static readonly BAD_DATE = 'Nelze vybrat pozdější datum nez je aktualní';
  public static readonly BAD_TIME = 'Rezervaci je nutné provést alespoň 3 hodiny dopředu';
  public static readonly RESERVATION_BEFORE_CLOSING_TIME = 'Rezervaci lze provést nejpozději 2 hodiny před zavírací dobou';
  public static readonly TIME_IS_NOT_IN_OPENING_HOURS = 'Vytváříte rezervaci mimo otevírací dobu';
  public static readonly TIME_IN_RESTAURANT = [
    {value: 0, viewValue: 'Nevím'},
    {value: 1, viewValue: '1 hodinu'},
    {value: 2, viewValue: '2 hodiny'},
    {value: 3, viewValue: '3 hodiny'},
    {value: 4, viewValue: '4 hodiny'},
    {value: 5, viewValue: '5 hodin'}
  ];
  public static readonly PLACE_IN_RESTAURANT = [
    {value: 'table', viewValue: 'Stůl'},
    {value: 'lounge', viewValue: 'Salonek'}
  ];
  public static readonly OPEN_AT = '10:00';
  public static readonly CLOSED_AT = '23:00';
}
