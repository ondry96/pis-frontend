export interface NewCategoryModel {
  name: string;
}

export interface ChefMenuModel {
  'categoriesInMenu': [
    Category
  ];
  'categories': [
    Category
  ];
}

export interface Category {
  'categoryName': string;
  'menuItems': [
    MenuItemInCategory
  ];
}

export interface MenuItemInCategory {
  'menuItemId': number;
  'name': string;
  'price': number;
  'grammage': number;
  'description': string;
}
