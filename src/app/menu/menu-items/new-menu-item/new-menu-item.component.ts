import { Component, OnInit } from '@angular/core';
import {ResponseErrorHandler} from '../../../../utils/ResponseErrorHandler';
import {MenuService} from '../../../../services/menu/menu.service';
import {AllCategoriesResponse} from '../../../response.model';
import {NewAndEditMenuItem} from '../menu-items.model';
import {NotifierDisplayerUtil} from '../../../../utils/NotifierDisplayerUtil';
import {Router} from '@angular/router';
import {MenuConstants} from '../../menu.constants';
import {ValuesCheckerUtil} from '../../../../utils/ValuesCheckerUtil';

@Component({
  selector: 'app-new-menu-item',
  templateUrl: './new-menu-item.component.html',
  styleUrls: [
    './new-menu-item.component.less',
    '../edit-menu-item/edit-menu-item.component.less',
    '../../menu.less'
  ]
})
export class NewMenuItemComponent implements OnInit {
  private menuConstants: typeof MenuConstants = MenuConstants;

  private categories: AllCategoriesResponse[] = [];
  private newMenuItem: NewAndEditMenuItem = {
    categoryId: 0,
    description: '',
    grammage: '',
    name: '',
    price: 100,
    productionPrice: 80
  };
  private chosenCategoryName;

  constructor(private menuService: MenuService, private router: Router) {
  }

  ngOnInit() {
    this.getAllCategories();
  }

  getAllCategories() {
    this.menuService.getAllCategories().subscribe(
      res => {
        this.categories = res;
        this.chosenCategoryName = this.categories[0].name;
      }, error => {
        ResponseErrorHandler.handleError(error);
      }
    );
  }

  sendAddNewMenuItem() {
    (document.getElementById('submitButtonNew') as HTMLButtonElement).disabled = true;
    this.newMenuItem.categoryId = this.categories.find(cat => cat.name === this.chosenCategoryName).categoryId;
    this.menuService.addNewMenuItem(this.newMenuItem).subscribe(
      res => {
        NotifierDisplayerUtil.showNotification('success', 'Pokrm byl úspěšně vytvořen.');
        this.router.navigate(['/menu-items']);
      }, error => {
        ResponseErrorHandler.handleError(error);
        (document.getElementById('submitButtonNew') as HTMLButtonElement).disabled = false;
      }
    );
  }

  private checkValues(): boolean {
    return ValuesCheckerUtil.checkPositivePrice(this.newMenuItem) &&
      ValuesCheckerUtil.checkEmptyGrammage(this.newMenuItem);
  }
}
