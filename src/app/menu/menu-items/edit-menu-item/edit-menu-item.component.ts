import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {MenuConstants} from '../../menu.constants';
import {NewAndEditMenuItem} from '../menu-items.model';
import {MenuService} from '../../../../services/menu/menu.service';
import {ResponseErrorHandler} from '../../../../utils/ResponseErrorHandler';
import {AllCategoriesResponse} from '../../../response.model';
import {NotifierDisplayerUtil} from '../../../../utils/NotifierDisplayerUtil';
import {ValuesCheckerUtil} from '../../../../utils/ValuesCheckerUtil';

@Component({
  selector: 'app-edit-menu-item',
  templateUrl: './edit-menu-item.component.html',
  styleUrls: [
    './edit-menu-item.component.less',
    '../../menu.less'
  ]
})
export class EditMenuItemComponent implements OnInit {
  private menuConstants: typeof MenuConstants = MenuConstants;
  private id;

  private editMenuItem: NewAndEditMenuItem = {
    categoryId: 0,
    description: '',
    grammage: '',
    name: '',
    price: 0,
    productionPrice: 0

  };

  private categories: AllCategoriesResponse[] = [];

  private chosenCategoryName;

  constructor(private activatedRoute: ActivatedRoute,
              private router: Router,
              private menuService: MenuService) {}

  ngOnInit() {
    this.id = Number(this.activatedRoute.snapshot.paramMap.get('id'));
    if (isNaN(this.id)) {
      this.router.navigate(['/menu-items/']);
      return;
    }
    this.getAllCategories();
    this.getMenuItem(this.id);
  }

  getMenuItem(id) {
    this.menuService.getMenuItem(id).subscribe(
      res => {
        this.editMenuItem.grammage = res.grammage;
        this.editMenuItem.name = res.name;
        this.editMenuItem.price = res.price;
        this.editMenuItem.productionPrice = res.productionPrice;
        this.editMenuItem.description = res.description;
        this.chosenCategoryName = res.categoryName;
      }, error => {
        this.router.navigate(['/menu-items/']);
        ResponseErrorHandler.handleError(error);
      }
    );
  }

  getAllCategories() {
    this.menuService.getAllCategories().subscribe(
      res => {
        this.categories = res;
      }, error => {
        this.router.navigate(['/menu-items/']);
        ResponseErrorHandler.handleError(error);
      }
    );
  }

  sendEditNewMenuItem() {
    (document.getElementById('submitButtonEdit') as HTMLButtonElement).disabled = true;
    this.editMenuItem.categoryId = this.categories.find(cat => cat.name === this.chosenCategoryName).categoryId;

    this.menuService.updateMenuItem(this.editMenuItem, this.id).subscribe(
      res => {
        NotifierDisplayerUtil.showNotification('success', 'Pokrm byl úspěšně změněn.');
        this.router.navigate(['/menu-items']);
      }, error => {
        ResponseErrorHandler.handleError(error);
        (document.getElementById('submitButtonEdit') as HTMLButtonElement).disabled = false;
      }
    );
  }

  private checkValues(): boolean {
    return ValuesCheckerUtil.checkPositivePrice(this.editMenuItem) &&
      ValuesCheckerUtil.checkEmptyGrammage(this.editMenuItem);
  }
}
