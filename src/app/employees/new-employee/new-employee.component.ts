import {Component, OnInit} from '@angular/core';
import {RegisterConstants} from '../../authentication/register/register.constants';
import {NewEmployeeModel} from './new-employee.model';
import {Router} from '@angular/router';
import {EmployeesService} from '../../../services/employees/employees.service';
import {EmployeesConstants} from '../employees.constants';
import {NotifierDisplayerUtil} from '../../../utils/NotifierDisplayerUtil';
import {ResponseErrorHandler} from '../../../utils/ResponseErrorHandler';

@Component({
  selector: 'app-new-employee',
  templateUrl: './new-employee.component.html',
  styleUrls: [
    '../employees.less',
    './new-employee.component.less'
  ]
})
export class NewEmployeeComponent implements OnInit {
  private readonly employeesConstants: typeof EmployeesConstants = EmployeesConstants;
  private readonly registerConstants: typeof RegisterConstants = RegisterConstants;

  newEmployeeModel: NewEmployeeModel = {
    employeeInfo: {
      name: '',
      surname: '',
      email: ''
    },
    roles: []
  };

  constructor(private employeesService: EmployeesService, private router: Router) {
    for (const role of this.employeesConstants.ROLES_ARRAY) {
      this.newEmployeeModel.roles.push(
        {
          name: role.name,
          hasRole: role.defaultValue
        }
      );
    }
  }

  ngOnInit() {
  }

  createNewEmployee() {
    (document.getElementById('submitButton') as HTMLButtonElement).disabled = true;
    this.employeesService.createNewEmployee(this.newEmployeeModel).subscribe(
      () => {
        NotifierDisplayerUtil.showNotification('success', 'Uživatel byl úspěšně vytvořen.');
        this.router.navigate(['employees/edit']);
      },
      error => {
        ResponseErrorHandler.handleError(error);
        (document.getElementById('submitButton') as HTMLButtonElement).disabled = false;
      }
    );
  }

  isAnyRoleSet() {
    return !!this.newEmployeeModel.roles.find(role => role.hasRole === true);
  }

}
