import {Component, OnInit} from '@angular/core';
import {EmployeesService} from '../../../services/employees/employees.service';
import {Employee, EmployeeInfo} from '../../response.model';
import {EmployeesConstants} from '../employees.constants';
import {
  EditEmployeeAttributeModel,
  ChangeRoleStateModel
} from './manage-employees.model';
import {NotifierDisplayerUtil} from '../../../utils/NotifierDisplayerUtil';
import {CheckboxOperationsUtil} from '../../../utils/CheckboxOperationsUtil';
import {ResponseErrorHandler} from '../../../utils/ResponseErrorHandler';
import {ArrayItemManipulatorUtil} from '../../../utils/ArrayItemManipulatorUtil';
import Swal from 'sweetalert2';
import {SweetalertInfoSetterUtil} from '../../../utils/SweetalertInfoSetterUtil';

@Component({
  selector: 'app-manage-employees',
  templateUrl: './manage-employees.component.html',
  styleUrls: [
    '../employees.less',
    './manage-employees.component.less'
  ]
})
export class ManageEmployeesComponent implements OnInit {

  private readonly employeeConstants: typeof EmployeesConstants = EmployeesConstants;
  private dataLoaded = false;
  employeesModel: Employee[];
  err = true;
  noEmployeesInDatabase = false;

  constructor(private employeesService: EmployeesService) {
  }

  ngOnInit() {
    this.getAllEmployees();
  }

  /* Sending and reacting to http requests */

  getAllEmployees() {
     this.employeesService.getAllEmployees().subscribe(
      res => {
        this.err = false;
        this.employeesModel = res;
        // delete of admin
        const employee = this.employeesModel.find(admin => admin.employeeInfo.email === this.employeeConstants.ADMIN_EMAIL);
        ArrayItemManipulatorUtil.deleteItemFromArray(employee, this.employeesModel);
        this.noEmployeesInDatabase = (this.employeesModel.length === 0);
        this.dataLoaded = true;
      },
      error => {
        this.err = true;
        ResponseErrorHandler.handleError(error);
      }
    );
  }

  removeEmployee(employee: EmployeeInfo, $event: MouseEvent) {
    Swal.fire(
      SweetalertInfoSetterUtil.getInfoForDeleteAlert('Opravdu chcete vymazat uživatele ' + employee.name + ' ' + employee.surname + '?')
    ).then((result) => {
      if (result.value) {
        (($event.target) as HTMLButtonElement).disabled = true;

        this.employeesService.deleteEmployee(employee.email).subscribe(
          () => {
            const employeeToDelete = this.findEmployeeByEmail(employee.email);
            ArrayItemManipulatorUtil.deleteItemFromArray(employeeToDelete, this.employeesModel);
            NotifierDisplayerUtil.showNotification('success',
              'Uživatel ' + employee.name + ' ' + employee.surname + ' úspěšně smazán.');
            this.ngOnInit();
          },
          error => {
            ResponseErrorHandler.handleError(error);
            (($event.target) as HTMLButtonElement).disabled = false;
          }
        );
      }
    });
  }

  private sendChangeRolePost(email: string, roleName: string) {
    this.employeesService.changeRoleStateToEmployee(email, new ChangeRoleStateModel(roleName)).subscribe(
      () => {
        NotifierDisplayerUtil.showNotification('success', 'Role byla změněna.');
      },
      error => {
        ResponseErrorHandler.handleError(error);
      }
    );
  }

  /* Handle checkbox clicks method */
  changeRoleState(employee: Employee, roleName: string, employeeId: number, checkBoxId: number) {
    const role = this.hasRoleByName(employee, roleName);
    if (role) {
      const index = employee.roles.indexOf(role, 0);
      if (index > -1) {
        if (employee.roles.length === 1) {
          NotifierDisplayerUtil.showNotification('danger', this.employeeConstants.CHANGE_COULD_NOT_BE_DONE);
          const checkbox = document.getElementById('checkbox-' + employeeId + '-' + checkBoxId) as HTMLInputElement;
          CheckboxOperationsUtil.uncheckForMoment(checkbox).then(
            () => checkbox.checked = true
          );
          return;
        }
        employee.roles.splice(index, 1);
      }
    } else {
      employee.roles.push(roleName);
    }

    this.sendChangeRolePost(employee.employeeInfo.email, roleName);
  }

  changeEmployeeAttribute(employeeEmail: string,  attribute: string, newValue: string) {
    const editEmployeeAttributeModel: EditEmployeeAttributeModel = {
      attribute,
      value: newValue
    };
    this.employeesService.editEmployeeAttribute(employeeEmail, editEmployeeAttributeModel).subscribe(
      () => {
        NotifierDisplayerUtil.showNotification('success', this.employeeConstants.CHANGED_SUCCESSFULLY);
        this.ngOnInit();
      },
      error => {
        ResponseErrorHandler.handleError(error);
        this.ngOnInit();
      }
    );
  }

  checkValidityOfInputValue(name: string, newValue: string) {
    if (newValue === null || newValue === '') {
      NotifierDisplayerUtil.showNotification('danger', this.employeeConstants.FILL_VALUE_TO_INPUT);
      this.ngOnInit();
      return false;
    }
    if (name === 'email' && !this.validateEmail(newValue)) {
      NotifierDisplayerUtil.showNotification('danger', this.employeeConstants.NOT_CORRECT_EMAIL_FORMAT);
      this.ngOnInit();
      return false;
    }
    return true;
  }


  /* Private logic methods */
  private hasRoleByName(employee: Employee, roleName: string) {
    return employee.roles.find(s => s === roleName);
  }

  private findEmployeeByEmail(email: string) {
    return this.employeesModel.find(s => s.employeeInfo.email === email);
  }

  private deleteEmployeeFromModel(employeeToDelete: Employee) {
    const index = this.employeesModel.indexOf(employeeToDelete, 0);
    if (index > -1) {
      this.employeesModel.splice(index, 1);
    }
  }

  private validateEmail(emailAddress) {
    const regularExpression = new RegExp(/^[^\s@]+@[^\s@]+$/);
    return regularExpression.test(emailAddress);
  }

}
