import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {ReservationInfo, CreateReservationModel, FinalReservation} from '../../app/reservation/create-reservation/create-reservation.model';
import {Observable} from 'rxjs';
import {GetFreeAndOccupiedTablesResponse, Reservation, ResponseModel} from '../../app/response.model';
import {ConstantsRoutesModel} from '../../app/constants-routes.model';
import {HeaderSetterUtil} from '../../utils/HeaderSetterUtil';
import {ReservationStateModel} from '../../app/reservation/reservations/reservation.model';
import {DatePipe} from '@angular/common';
import {CreateReservationConstants} from '../../app/reservation/create-reservation/create-reservation.constants';
import {DeleteReservationCodeModel} from '../../app/reservation/delete-reservation-code/delete-reservation-code.model';

@Injectable({
  providedIn: 'root'
})
export class ReservationService {

  private readonly urlRoutesConstants: typeof ConstantsRoutesModel = ConstantsRoutesModel;

  constructor(private http: HttpClient) {
  }

  sendNewReservationUnloggedUserData(reservation: CreateReservationModel): Observable<ResponseModel> {
    return this.http.post<any>(this.urlRoutesConstants.NEW_RESERVATION_UNLOGGED_URL, reservation);
  }

  getAllReservations(): Observable<Reservation[]> {
    return this.http.get<any>(this.urlRoutesConstants.ALL_RESERVATIONS, HeaderSetterUtil.setAuthorization());
  }

  sendChangeOfState(reservationState: ReservationStateModel, id: number): Observable<ResponseModel> {
    return this.http.put<any>(
      this.urlRoutesConstants.CHANGE_RESERVATION_STATE + id, reservationState, HeaderSetterUtil.setAuthorization()
    );
  }

  sendFirstStepReservationLogged(reservation: ReservationInfo): Observable<GetFreeAndOccupiedTablesResponse> {
    return this.http.post<any>(
      this.urlRoutesConstants.NEW_RESERVATION_LOGGED_GET_FREE_PLACES, reservation, HeaderSetterUtil.setAuthorization()
    );
  }

  sendFullReservationLoggedOrWaiter(reservation: FinalReservation): Observable<ResponseModel> {
    return this.http.post<any>(
      this.urlRoutesConstants.NEW_RESERVATION_LOGGED_FINAL_STEP, reservation, HeaderSetterUtil.setAuthorization());
  }

  sendCodeForDeleteReservation(code: DeleteReservationCodeModel): Observable<ResponseModel> {
    return this.http.post<any>(this.urlRoutesConstants.DELETE_RESERVATION_VIA_CODE_URL, code);
  }

  isDateRight(date: string, datePipe: DatePipe) {
    return date >= datePipe.transform(new Date(), 'yyyy-MM-dd');
  }

  isHostsCountRight(hostsCount: number) {
    return hostsCount > 0 && hostsCount <= 40;
  }

  isTimeRight(date: string, time: string, datePipe: DatePipe) {
    /*rezervaci lze provest v casech 10:00 az 23:00*/

    if (!(date >= datePipe.transform(new Date(), 'yyyy-MM-dd'))) {
      return false;
    } else {
      if (date === datePipe.transform(new Date(), 'yyyy-MM-dd')) {
        return time >= this.validTimeOfReservation(datePipe.transform(new Date(), 'HH:mm'));
      }
      return true;
    }
  }

  validTimeOfReservation(time: string) {
    let x = Number(time.charAt(1)) + 3;
    let a = time;
    if (x === 10) {
      x = 0;
    } else if (x === 11) {
      x = 1;
    } else if (x === 12) {
      x = 2;
    }

    if (x === 0 || x === 1 || x === 2) {
      const x1 = Number(time.charAt(0)) + 1;
      a = this.setCharAt(a, 0, x1.toString());
    }
    a = this.setCharAt(a, 1, x.toString());
    return a;
  }

  setCharAt(str: string, index: number, char: string) {
    if (index > str.length + 1) {
      return str;
    }
    return str.substr(0, index) + char + str.substr(index + 1);
  }

  isTimeInOpeningHours(time: string) {
    return time >= CreateReservationConstants.OPEN_AT &&
      time <= CreateReservationConstants.CLOSED_AT;
  }

  isTimeBeforeClosing(time: string) {
    return time >= '21:01' && time <= '23:00';
  }
}
