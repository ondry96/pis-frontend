import { Injectable } from '@angular/core';
import {NewEmployeeModel} from '../../app/employees/new-employee/new-employee.model';
import {Observable} from 'rxjs';
import {Employee, ResponseModel} from '../../app/response.model';
import {HttpClient} from '@angular/common/http';
import {ConstantsRoutesModel} from '../../app/constants-routes.model';
import {
  EditEmployeeAttributeModel,
  ChangeRoleStateModel
} from '../../app/employees/manage-employees/manage-employees.model';
import {HeaderSetterUtil} from '../../utils/HeaderSetterUtil';

@Injectable({
  providedIn: 'root'
})
export class EmployeesService {
  private readonly urlRoutesConstants: typeof ConstantsRoutesModel = ConstantsRoutesModel;

  constructor(private http: HttpClient) { }

  createNewEmployee(newEmployeeModel: NewEmployeeModel): Observable<ResponseModel> {
    return this.http.post<any>(this.urlRoutesConstants.NEW_EMPLOYEE_URL, newEmployeeModel, HeaderSetterUtil.setAuthorization());
  }

  getAllEmployees(): Observable<Employee[]> {
    return this.http.get<any>(this.urlRoutesConstants.EDIT_EMPLOYEES_GET_URL, HeaderSetterUtil.setAuthorization());
  }

  changeRoleStateToEmployee(employeeEmail: string, changeRoleStateModel: ChangeRoleStateModel): Observable<ResponseModel> {
    return this.http.put<any>(this.urlRoutesConstants.EMPLOYEE_BASE_URL + employeeEmail +
      this.urlRoutesConstants.CHANGE_ROLE_STATE, changeRoleStateModel, HeaderSetterUtil.setAuthorization());
  }

  deleteEmployee(employeeEmail: string): Observable<ResponseModel> {
    return this.http.delete<any>(this.urlRoutesConstants.EMPLOYEE_BASE_URL + employeeEmail +
                                     this.urlRoutesConstants.DELETE, HeaderSetterUtil.setAuthorization());
  }

  editEmployeeAttribute(employeeEmail: string, editEmployeeAttributeModel: EditEmployeeAttributeModel): Observable<ResponseModel> {
    return this.http.put<any>(this.urlRoutesConstants.EMPLOYEE_BASE_URL + employeeEmail +
      this.urlRoutesConstants.EDIT_ATTRIBUTE, editEmployeeAttributeModel, HeaderSetterUtil.setAuthorization());
  }
}
