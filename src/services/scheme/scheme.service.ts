import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {SchemeModel} from '../../app/reservation/create-reservation/restaurant-reservation-scheme/scheme.model';
import {HttpClient} from '@angular/common/http';
import {ConstantsRoutesModel} from '../../app/constants-routes.model';
import {HeaderSetterUtil} from '../../utils/HeaderSetterUtil';

@Injectable({
  providedIn: 'root'
})
export class SchemeService {
  private readonly urlRoutesConstants: typeof ConstantsRoutesModel = ConstantsRoutesModel;

  constructor(private http: HttpClient) {
  }

  getAllTables(): Observable<SchemeModel[]> {
    return this.http.get<any>(this.urlRoutesConstants.GET_ALL_TABLES, HeaderSetterUtil.setAuthorization());
  }

  getAllLounges(): Observable<SchemeModel[]> {
    return this.http.get<any>(this.urlRoutesConstants.GET_ALL_LOUNGES, HeaderSetterUtil.setAuthorization());
  }
}
