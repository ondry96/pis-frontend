import {Injectable} from '@angular/core';
import {ConstantsRoutesModel} from '../../app/constants-routes.model';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {HeaderSetterUtil} from '../../utils/HeaderSetterUtil';
import {AllCategoriesResponse, ResponseModel, TokenResponseModel} from '../../app/response.model';
import {Category, ChefMenuModel, NewCategoryModel} from '../../app/menu/menu/chef-menu/chef-menu.model';
import {MenuItem, NewAndEditMenuItem} from '../../app/menu/menu-items/menu-items.model';

@Injectable({
  providedIn: 'root'
})
export class MenuService {

  private readonly urlRoutesConstants: typeof ConstantsRoutesModel = ConstantsRoutesModel;

  constructor(private http: HttpClient) {
  }

  getAllCategories(): Observable<AllCategoriesResponse[]> {
    return this.http.get<any>(this.urlRoutesConstants.CATEGORIES, HeaderSetterUtil.setAuthorization());
  }

  addCategory(newCategoryModel: NewCategoryModel): Observable<ResponseModel> {
    return this.http.post<any>(this.urlRoutesConstants.CATEGORIES, newCategoryModel, HeaderSetterUtil.setAuthorization());
  }

  deleteCategory(categoryName: string): Observable<ResponseModel> {
    return this.http.delete<any>(this.urlRoutesConstants.CATEGORIES + categoryName, HeaderSetterUtil.setAuthorization());
  }

  getMenuInCategories(): Observable<ChefMenuModel> {
    return this.http.get<any>(this.urlRoutesConstants.EDIT_MENU, HeaderSetterUtil.setAuthorization());
  }

  editMenuCategories(model: ChefMenuModel): Observable<ResponseModel> {
    return this.http.post<any>(this.urlRoutesConstants.EDIT_MENU, model, HeaderSetterUtil.setAuthorization());
  }

  getAllMenuItems(): Observable<[Category]> {
    return this.http.get<any>(this.urlRoutesConstants.MENU_ITEMS, HeaderSetterUtil.setAuthorization());
  }

  deleteMenuItem(menuItemId: number): Observable<ResponseModel> {
    return this.http.delete<any>(this.urlRoutesConstants.MENU_ITEMS + menuItemId, HeaderSetterUtil.setAuthorization());
  }

  addNewMenuItem(newMenuModel: NewAndEditMenuItem): Observable<ResponseModel> {
    return this.http.post<any>(this.urlRoutesConstants.ADD_MENU_ITEM, newMenuModel, HeaderSetterUtil.setAuthorization());
  }

  getMenuItem(idMenuItem: number): Observable<MenuItem> {
    return this.http.get<any>(this.urlRoutesConstants.MENU_ITEMS + idMenuItem, HeaderSetterUtil.setAuthorization());
  }

  updateMenuItem(editedMenuItem: NewAndEditMenuItem, idMenuItem: number): Observable<TokenResponseModel> {
    return this.http.put<any>(this.urlRoutesConstants.MENU_ITEMS + idMenuItem,
      editedMenuItem, HeaderSetterUtil.setAuthorization());
  }

  getAllMenuItemsInMenu(): Observable<[Category]> {
    return this.http.get<any>(this.urlRoutesConstants.MENU_ITEMS_IN_MENU);
  }
}
