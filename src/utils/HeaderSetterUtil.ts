import {HttpHeaders} from '@angular/common/http';

export class HeaderSetterUtil {

  static setAuthorization() {
    const token = localStorage.getItem('token');
    return {
      headers: new HttpHeaders()
        .set('Authorization',  `Bearer ${token}`)
    };
  }
}
