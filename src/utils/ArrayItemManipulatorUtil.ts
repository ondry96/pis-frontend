export class ArrayItemManipulatorUtil {

  static deleteItemFromArray(item: any, array: Array<any>) {
    const index = array.indexOf(item, 0);
    if (index > -1) {
      array.splice(index, 1);
    }
  }
}
