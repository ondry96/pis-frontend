export class ActionButtonUtil {

  static setStyle() {
    const buttons = (document.getElementsByClassName('action-button') as HTMLCollectionOf<HTMLElement>);

    // @ts-ignore
    for (const button of buttons) {
      this.unsetStyle(button);
      const siblingTd = button.parentElement.firstElementChild;

      const computedStyle = getComputedStyle(siblingTd);
      let elementHeight = siblingTd.clientHeight;  // height with padding
      elementHeight -= parseFloat(computedStyle.paddingTop) + parseFloat(computedStyle.paddingBottom);

      button.style.height = elementHeight + 16 + 'px'; // 16 = padding-top + padding-bottom
    }
  }

  static unsetStyle(button: HTMLElement) {
    button.style.height = '';
  }
}
