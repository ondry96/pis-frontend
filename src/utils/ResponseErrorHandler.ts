import {NotifierDisplayerUtil} from './NotifierDisplayerUtil';

export class ResponseErrorHandler {

  static handleError(error: any) {
    if (error.error.message !== undefined) {
      NotifierDisplayerUtil.showNotification('danger', error.error.message);
    } else {
      NotifierDisplayerUtil.showNotification('danger', 'Vnitřní chyba serveru.');
    }
  }
}
