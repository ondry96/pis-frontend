export class CheckboxOperationsUtil {

  static uncheckForMoment(checkbox: HTMLInputElement) {
    return new Promise(resolve => {
      setTimeout(() => {
        resolve( checkbox.checked = false);
      }, 600);
    });
  }
}
