import Swal, {SweetAlertOptions} from 'sweetalert2';
import {MenuItem} from '../app/menu/menu-items/menu-items.model';

export class SweetalertInfoSetterUtil {

  static getConfirmAlert(title: string): SweetAlertOptions {
    return {
      title,
      icon: 'question',
      showCancelButton: true,
      confirmButtonColor: '#87CB16',
      cancelButtonColor: '#FF4A55',
      confirmButtonText: 'Ano',
      cancelButtonText: 'Ne'
    };
  }

  static getInfoForDeleteAlert(title: string): SweetAlertOptions {
    return {
      title,
      text: 'Tuto akci nelze vzít zpět',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#FF4A55',
      cancelButtonColor: '#1DC7EA',
      confirmButtonText: 'Ano',
      cancelButtonText: 'Ne'
    };
  }

  static getMenuItemFullInfoAlert(menuItem: MenuItem): SweetAlertOptions {
    return {
      title: '<strong>' + menuItem.name + '</strong>',
      html:
        '<div>' +
        '<table class="table" style="text-align: left">' +
        '<tr><td><b>Kategorie pokrmu:</b></td><td>' + menuItem.categoryName + '</td></tr>' +
        '<tr><td><b>Prodejní&nbsp;cena:</b></td><td>' + menuItem.price + '&nbsp;Kč</td></tr>' +
        '<tr><td><b>Výrobní&nbsp;cena:</b></td><td>' + menuItem.productionPrice + '&nbsp;Kč</td></tr>' +
        '<tr><td><b>Množství:</b></td><td>' + menuItem.grammage + '</td></tr>' +
        '<tr><td><b>Popisek:</b></td><td>' + menuItem.description + '</td></tr>' +
        '</table>' +
        '</div>',
      showCloseButton: true,
      showConfirmButton: false
    };
  }

  static waitForReservationCreation(type, message: any) {
    switch (type) {
      case 'load':
        Swal.fire({
          title: 'Vytváření rezervace',
          text: 'Prosím, počkejte',
          imageUrl: 'assets/img/loading.gif',
          customClass: {image: 'swal-loading-gif'},
          imageWidth: 88,
          imageHeight: 88,
          showConfirmButton: false,
          allowOutsideClick: false
        });
        break;
      case false:
        Swal.fire({
          title: 'Rezervace nebyla vytvořena',
          text: message.error.message,
          icon: 'error',
          // confirmButtonColor: '#87CB16',
          confirmButtonText: 'Zavřít',
        });
        break;
      case true:
        Swal.fire({
          title: message,
          text: 'Těšíme se na Vaši návštěvu',
          icon: 'success',
          showConfirmButton: false,
        });
        setTimeout(() => {
          Swal.close();
        }, 3000);
        break;
      default:
        console.error('Wrong type');
        break;
    }
  }
}
